server {
	listen 80;
	
	root /usr/share/nginx/www;
	index index.html index.php;
	
	server_name localhost;

	location / {
		try_files $uri $uri/ /index.html;
	}

	error_page 404 /404.html;
	
	error_page 500 502 503 504 /50x.html;
		location = /50x.html {
			root /usr/share/nginx/www;
	}

	location ~ \.php$ {
		fastcgi_split_path_info ^(.+\.php)(/.+)$;
		fastcgi_pass 127.0.0.1:9000;
		fastcgi_index index.php;
		include fastcgi_params;
	}
}
